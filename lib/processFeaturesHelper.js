function processFeatureObjectHelper(feature, config) {
  const featureCopy = { ...feature };
  featureCopy.aos_animation = config.card_animation;
  featureCopy.aos_duration = config.card_animation_duration;
  const isCore =
    feature.gitlab_free ||
    (!(
      feature.gitlab_com === false || feature.gitlab_com === 'not_applicable'
    ) &&
      feature.gitlab_core);
  const isSilver =
    feature.gitlab_silver ||
    (!(
      feature.gitlab_com === false || feature.gitlab_com === 'not_applicable'
    ) &&
      feature.gitlab_premium);
  const isGold =
    feature.gitlab_gold ||
    (!(
      feature.gitlab_com === false || feature.gitlab_com === 'not_applicable'
    ) &&
      feature.gitlab_ultimate);
  featureCopy.isSelfManaged =
    (feature.gitlab_core && !isCore) ||
    (feature.gitlab_premium && !isSilver) ||
    (feature.gitlab_ultimate && !isGold);
  featureCopy.isFree = feature.gitlab_core || isCore;
  featureCopy.isPremium = feature.gitlab_premium || isSilver;
  featureCopy.isUltimate = feature.gitlab_ultimate || isGold;

  return featureCopy;
}

function filterFeaturesHelper(featuresArray, filter, value) {
  return featuresArray.filter((feature) => {
    return (
      feature?.[filter]?.includes(value) &&
      (feature.gitlab_core ||
        feature.gitlab_premium ||
        feature.gitlab_ultimate ||
        feature.gitlab_free ||
        feature.gitlab_silver ||
        feature.gitlab_gold)
    );
  });
}

module.exports = {
  processFeatureObjectHelper,
  filterFeaturesHelper,
};
