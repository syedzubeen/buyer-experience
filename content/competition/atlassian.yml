---
  title: GitLab vs Atlassian
  hero:
    title: GitLab vs Atlassian
    subtitle: How does GitLab compare to Atlassian in the Plan stage?
    icon:
      name: plan
      alt: plan Icon
      variant: marketing
      size: md
    crumbs:
      - title: DevOps maturity comparison
        href: /competition/
        data_ga_name: Competition
        data_ga_location: breadcrumb
      - title: GitLab vs. Atlassian
  overview:
    comparisons:
      - title: GitLab
        image: /nuxt-images/developer-tools/harvey-balls/75.svg
      - title: Jira
        image: /nuxt-images/developer-tools/harvey-balls/100.svg
    left_card:
      title: Jira helps teams plan, assign, track, report, and manage work
      description: |
        Atlassian is best known for Jira, its agile software project management platform. Atlassian’s strategy centers around a comprehensive digital product pipeline, including planning, collaboration, task and product development (Jira and Trello); collaborative source control and continuous delivery (Bitbucket); knowledge management (Confluence); and operations (Jira Service Management).

        In comparison to Jira (Plan stage), we take into consideration three of the Atlassian products that map 1:1 to the defined features: Jira, Jira Align, and Jira Service Management. Jira + Jira Align remains a leading product in the market. It is a full-featured solution with built-in support for multiple agile frameworks. When placing GitLab versus Atlassian in Portfolio planning, Jira Align outpaces GitLab when it comes to an enterprise agile planning platform that connects work to product, program and portfolio management at scale. With Gitlab versus Jira Service Management (Service Desk), Jira is seen as a leader in Enterprise Service Management due to the evolution of Jira Service Desk into Jira Service Management via various acquisitions.

        GitLab’s most notable competitive advantage over Jira is in Team Planning. GitLab offers robust team planning capabilities that allow work to be linked directly to the DevOps lifecycle. Keeping teams in a single application offers operational and financial advantages. Many customers inquire about what a transition would look like from Jira Software to GitLab to team planning. Jira has limited to no features in Requirements Management, Design Management and Quality Management, and GitLab outpaces them in these feature areas.

    right_card:
      title: GitLab's product roadmap
      bullets:
        - title: "[Transition Plan features to ProjectNamespace](/direction/dev/#whats-next-for-us)"
        - title: "[Saved views](https://gitlab.com/groups/gitlab-org/-/epics/5516) and [grid views](https://gitlab.com/gitlab-org/gitlab/-/issues/323095) - Boards, Lists, and Grids become views that are easily accessed and saved. This thread of work sets the stage for executive rollups."
        - title: "[Share resources (work items, milestones, labels, and so on) with multiple projects or groups](https://gitlab.com/gitlab-org/gitlab/-/issues/296668)"
        - title: "[Configurable work item statuses](https://gitlab.com/groups/gitlab-org/-/epics/5099)"
        - title: "[Team velocity and volatility](https://gitlab.com/groups/gitlab-org/-/epics/435)"
        - title: "[Iteration & Milestone Reporting Enhancements](https://about.gitlab.com/direction/dev/#whats-next-for-us)"
        - title: "[OKRs and rollups](https://gitlab.com/gitlab-org/gitlab/-/issues/273259) - Enable OKRs, Initiatives, and other executive-level work items into critical views."
      button:
        text: GitLab releases
        link: /releases/
  analysis:
    side_by_side:
      stage: Plan
      icon:
        name: plan
        alt: Plan Icon
        variant: marketing
        size: md
      tabs:
        - title: Team Planning
          panes:
            - title: GitLab
              harveyball: /nuxt-images/developer-tools/harvey-balls/50.svg
              description: Plan, organize, and track team progress using Scrum, Kanban, SAFe, and other Agile methodologies.
              sections:
                - title: Details
                  content: |
                    * Scrum/Kanban: The GitLab issue board is a software project management tool used to plan, organize, and visualize a workflow for a feature or product release. It can be used as a [Kanban](https://docs.gitlab.com/ee/user/project/issue_board.html#group-issues-in-swimlanes) or a [Scrum](https://docs.gitlab.com/ee/user/project/issue_board.html) board.
                    * SAFe and Other Agile Methodologies: Flexible Enterprise Agile Framework support for any [Team of Team](https://about.gitlab.com/blog/2020/11/11/gitlab-for-agile-portfolio-planning-project-management/#agile-software-development-at-scale) framework.  Achieved with a combination of: deeply nested org structure, [multi-level epics](https://docs.gitlab.com/ee/user/group/epics/manage_epics.html#multi-level-child-epics) and flexible time boxes like [iterations, milestones](https://docs.gitlab.com/ee/user/group/iterations/) and [roadmap](https://docs.gitlab.com/ee/user/group/roadmap/) for portfolio level views of dates. Health status allows roll up of [status at epic](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#health-status). Does not natively support SAFe.
                    * [Burnup/Burndown](https://docs.gitlab.com/ee/user/project/milestones/burndown_and_burnup_charts.html) charts show the progress of completing a milestone
                    * [Insights](https://docs.gitlab.com/ee/user/project/insights/): Configure the Insights that matter for your projects to explore data such as triage hygiene, issues created/closed per a given period, average time for merge requests to be merged and much more.

                - title: Improving our product capabilities
                  content: |
                    * We will add more out-of-the-box functionality that aligns to industry-standard program and portfolio management, including frameworks like SAFe, LeSS, and Disciplined Agile.
                    * Today, GitLab uses many different objects to facilitate planning and tracking work. Over the next year, we will be converting all of these into a standard underlying work item with robust support for effortlessly modeling different roll-up hierarchies and relationships depending on which agile methodology an organization is currently utilizing.
                    * We will also be introducing flexible, embeddable saved views and queries that will allow teams and individual personas to visualize and track work in the way that works best for their needs and particular use cases. These advancements will enable organizations to use a set of robust primitives to solve the majority of enterprise planning and portfolio management use cases, from linking work items with value opportunities, OKRs, and outcome metrics to designing and tracking the ROI of value streams with complex dependency chains across the organization.

              button:
                title: Documentation
                link: https://docs.gitlab.com/ee/topics/plan_and_track.html#team-planning
            - title: Jira
              harveyball: /nuxt-images/developer-tools/harvey-balls/100.svg
              description: Break the big ideas down into manageable chunks across teams with user stories, issues, and tasks.
              sections:
                - title: Details
                  content: |
                    * [Scrum](https://support.atlassian.com/jira-software-cloud/docs/use-your-scrum-backlog/) template: Provides your team with all the capabilities it needs to break down large, complex projects into manageable pieces of work.
                    * [Kanban](https://support.atlassian.com/jira-software-cloud/docs/use-your-kanban-backlog/) template: Kanban in Jira Software helps teams easily design, manage and improve their workflow while providing transparency as work moves from to-do to done.
                    * [SAFe](https://confluence.atlassian.com/confeval/other-atlassian-evaluator-resources/atlassian-products-and-safe): Comprehensive support for the framework, including multi tier value streams, program room for Program Increment (PI) Planning, and checklists to guide users through tasks
                    * [Dashboards](https://www.atlassian.com/blog/jira-software/7-steps-to-a-beautiful-and-useful-agile-dashboard): Team tracking widgets that are all located in one central location. Allows for default or custom dashboards to be created including: Issue Statistics, Burnup/Burndown, Velocity, Cycle Time, Deployment Frequency and Cumulative flow Diagrams
                    * Has a “Deployments” feature that connects the issue to the MR/PR with tools like GitLab/GitHub to add further visibility into the SDLC
                    * Jira + Jira Align remains a leading product in the market. It is a full-featured solution with built-in support for multiple agile frameworks
              button:
                title: Documentation
                link: https://support.atlassian.com/jira-software-cloud/resources/
        - title: Portfolio Management
          panes:
            - title: GitLab
              harveyball: /nuxt-images/developer-tools/harvey-balls/50.svg
              description: Plan upcoming work by creating Epics and mapping all relevant Issues to them. Create and track against multiple milestones at the portfolio level to see status overtime and review progress towards your goals
              sections:
                - title: Details
                  content: |
                    * Strategic Alignment and Tracking: Epic boards, Epic Swimlanes, Health Status and Roadmaps give [portfolio-level views](https://docs.gitlab.com/ee/topics/plan_and_track.html#portfolio-management) of GitLab planning data
                    * Does not have Software Engineering or Portfolio Financial Management capabilities. Currently possible to achieve via [API](https://docs.gitlab.com/ee/api/custom_attributes.html)
                    * [End to End Visibility to the Value Stream](https://docs.gitlab.com/ee/user/analytics/#group-level-analytics). Instance-level analytics make it possible to aggregate analytics across GitLab, so that users can view information across multiple projects and groups in one place.
                    * Release Forecasting can be utilized with the [milestones](https://docs.gitlab.com/ee/user/project/milestones/)
                    * Epic and/or Feature Level Forecasting is driven via [Roadmaps](https://docs.gitlab.com/ee/user/group/roadmap/)
                - title: Improving our product capabilities
                  content: |
                    * Use DevOps Data To Guide Planning - GitLab is uniquely positioned as an end-to-end DevOps platform to deliver a planning suite that enables business leaders to drive their vision and empower their development teams to work efficiently.
                    * Today, engineering and product managers need to parse through information in multiple systems to know what problems will keep their teams from meeting their goals. The data is often hard to understand, so engineers' days are interrupted with requests for status updates, and executives rely on manually created status reports that are often inaccurate. To fully solve this problem, we will display DevOps data within Plan that is easy to understand for all personas, including non-developers.
                    * Our unification of the value stream allows us to interlink data across every stage of development, from initial analysis to planning, implementation, deployment, and monitoring. By leveraging data from across the entire value stream, including DORA4 metrics, flow analytics, work item type distribution, and iteration velocity and volatility, we will provide timely context to organizational leaders and teams that enable them to derive unique insights on the status and expected delivery dates of mission critical work streams so that they can make effective trade-off decisions and run complex "what if'' scenarios.

              button:
                title: Documentation
                link: https://docs.gitlab.com/ee/topics/plan_and_track.html#portfolio-management
            - title: Jira
              harveyball: /nuxt-images/developer-tools/harvey-balls/100.svg
              description: Jira Align is an Enterprise Agile Planning platform that connects work to product, program and portfolio management at scale.
              sections:
                - title: Details
                  content: |
                    * Real-time connected roadmaps - real-time, visual product roadmaps with progress to date that you can take directly to the executive and delivery teams.
                    * Resource Allocation - manage planned versus actual allocation for each program, team, and team member.
                    * Work in Progress - Manage how much work is in progress at the epic, feature, and story level by release, product, and / or team.
                    * Investments vs Actuals - View the desired portfolio investment, the PI allocation, and the actual spend per theme in one report.
                    * Strategic and Financial Alignment - Allocate investments to strategic themes and epics needed by the business to deliver financial results.
                    * SAFe Portfolio-level support - Portfolio Team alignment, value stream management, WSJF epic prioritization, enterprise forecasting, epic success criteria tracking, and built-in WIP reporting.
                    * Offers role based menu in product: Enterprise, Portfolio, Program, Team
                    * Dependency Maps that highlight bottlenecks
                    * Executive reporting that span across Portfolios
                    * Jira align must be purchased in addition to Jira Software to support these capabilities. These two together are a full-featured solution with built-in support for multiple agile frameworks
              button:
                title: Documentation
                link: https://help.jiraalign.com/hc/en-us/categories/204185927-Portfolio-level
        - title: Service Desk
          panes:
            - title: GitLab
              harveyball: /nuxt-images/developer-tools/harvey-balls/50.svg
              description: Connect your team using GitLab issues, to external parties directly via email for feedback and support, with no additional tools required.
              sections:
                - title: Details
                  content: |
                    * GitLab [Service Desk](https://docs.gitlab.com/ee/user/project/service_desk.html) enables people to create issues in your GitLab instance without needing their own user account (e.g. email).
                - title: Improving our product capabilities
                  content: |
                    * Over the next year, the Plan stage will be focused on consolidating Issues, Requirements and Epics into Work Items. Rationalizing the backend implementation will allow us to build Plan functionality more efficiently in the future. Due to that focus, we will not be dedicating significant capacity to the Service Desk category.Items that we may tackle in the future as more capacity becomes available are:
                      * Customize Service Desk to reflect our business, not GitLab specifically
                        * Outcome: Allow for a more seamless experience between the support team and the customer
                    * Service Desk functionality insufficient for IT Helpdesk use
                      * Outcome: Improve communication between customers and support team

              button:
                title: Documentation
                link: https://docs.gitlab.com/ee/user/project/service_desk.html
            - title: Jira
              harveyball: /nuxt-images/developer-tools/harvey-balls/100.svg
              description: Jira Service Management provides deeper service management practices across service request, incident, problem, change, knowledge, asset, and configuration management.
              sections:
                - title: Details
                  content: |
                    * Service Request Management helps organizations standardize the way they respond, coordinate, and fulfill service requests (e.g. email)
                    * Incident Management: responding to an unplanned event or service interruption and restoring the service to its operational state (e.g. service level agreements (SLAs) for incident records). Can integrate with Slack for incident notifications
                    * Problem Management: identifying and managing the causes of incidents, in order to reduce the number and impact of future incidents. Can be done through issues and aligned to a ‘problem’ workflow.
                    * Change Management: a service management practice designed to minimize risks and disruptions to IT services while making changes to critical systems and services. Utilizes native change management workflow, enforced approval, auto-approval for standard changes and can be viewed on a change calendar.
                    * Enterprise Service Management extends IT Service Management (ITSM) processes and tools across an organization to all teams. Can be done with purpose-built templates aligned to specific business teams.

              additional:
                title: Additional
                content: |
                  * Atlassian is seen as a leader in Enterprise Service Management due to the evolution of Jira Service Desk into Jira Service Management, including features of the former Jira Service Desk, Opsgenie, and Statuspage. Recent acquisitions of Mindville and ThinkTilt closed gaps in UI and data management capabilities, including configuration management database (CMDB).
              button:
                title: Documentation
                link: https://www.atlassian.com/software/jira/service-management/product-guide/overview
        - title: Requirements Management
          panes:
            - title: GitLab
              harveyball: /nuxt-images/developer-tools/harvey-balls/25.svg
              description: Gather and manage the use cases and requirements to meet business objectives.
              sections:
                - title: Details
                  content: |
                    * With [requirements](https://docs.gitlab.com/ee/user/project/requirements/?_gl=1*194wxzo*_ga*NDA5ODE5NDAwLjE2NjE0NjMwMTE.*_ga_ENFH3X7M5Y*MTY2MTc2MDIzMi42LjEuMTY2MTc2MjE5Ny4wLjAuMA..), you can set criteria to check your products against. They can be based on users, stakeholders, system, software, or anything else you find important to capture. A requirement is an artifact in GitLab which describes the specific behavior of your product. Requirements are long-lived and don’t disappear unless manually cleared.
                - title: Improving our product capabilities
                  content: |
                    * While these tools may be necessary for complex system level requirement work, we believe that managing requirements within GitLab can offer a much better user experience for individual teams who are not trying to integrate numerous complex systems.
              button:
                title: Documentation
                link: https://docs.gitlab.com/ee/user/project/requirements/
            - title: Jira
              harveyball: /nuxt-images/developer-tools/harvey-balls/25.svg
              description: JIRA for requirements management can be used in conjunction with Confluence for Requirements Management
              sections:
                - title: Details
                  content: |
                    * You're able to create a JIRA issue type specifically for requirements with its own workflow, custom fields and reporting. Sub-tasks offer a quick way to add and manage your requirements, and you can link related requirements together or with feature requests.
                    * Confluence integrates seamlessly with JIRA, allowing you to track your requirements in JIRA, linked to your corresponding project documentation in Confluence. To facilitate documenting your requirements, Confluence ships with a Blueprint template for requirements writing.
                    * Atlassian Marketplace has multipleRequirements Management apps which are tailored to workflows.
              additional:
                title: Additional
                content: |
                  * Requirements Management is not the top priority of Atlassian product suites. Products such as JAMA focus more primarily on Requirements Management.
              button:
                title: Documentation
                link: https://confluence.atlassian.com/jirakb/using-jira-for-requirements-management-193300521.html
        - title: Quality Management
          panes:
            - title: GitLab
              harveyball: /nuxt-images/developer-tools/harvey-balls/25.svg
              description:  Plan and track testing and quality of your product.
              sections:
                - title: Details
                  content: |
                    * [Test cases](https://docs.gitlab.com/ee/ci/test_cases/) in GitLab can help your teams create testing scenarios in their existing development platform.
                    * [Review Apps](https://docs.gitlab.com/ee/ci/examples/end_to_end_testing_webdriverio/,) are great: for every merge request (or branch, for that matter), the new code can be copied and deployed to a fresh production-like live environment, reducing the effort to assess the impact of changes
                    * You can configure your job to use [Unit test reports](https://docs.gitlab.com/ee/ci/unit_test_reports.html), and GitLab displays a report on the merge request so that it’s easier and faster to identify the failure without having to check the entire log.
                    * [Defect Tracking](https://docs.gitlab.com/ee/user/project/issues/) by using issues to collaborate on defects, solve problems, and plan remediation.
                - title: Improving our product capabilities
                  content: |
                    * The first step in building out Quality Management is a scaffolding framework for testing. In particular, we are calling these test cases, and test sessions. These will be first class native objects in GitLab, used to track the quality process of testing itself. The MVC can be seen at [https://gitlab.com/groups/gitlab-org/-/epics/3852](https://gitlab.com/groups/gitlab-org/-/epics/3852). Over the next year, the Plan stage will be focused on consolidating Issues, Requirements and Epics into Work Items.
              button:
                title: Documentation
                link: https://docs.gitlab.com/ee/ci/test_cases/index.html
            - title: Jira
              harveyball: /nuxt-images/developer-tools/harvey-balls/0.svg
              description: Jira does not offer this
        - title: Design Management
          panes:
            - title: GitLab
              harveyball: /nuxt-images/developer-tools/harvey-balls/50.svg
              description: Upload design assets to GitLab issues for easy collaboration on designs with a single source of truth.
              sections:
                - title: Details
                  content: |
                    * [Design Management](https://docs.gitlab.com/ee/user/project/issues/design_management.html?_gl=1*1juy1wp*_ga*NDA5ODE5NDAwLjE2NjE0NjMwMTE.*_ga_ENFH3X7M5Y*MTY2MTc2MDIzMi42LjEuMTY2MTc2MjQ1My4wLjAuMA..) you can upload design assets (including wireframes and mockups) to GitLab issues and keep them stored in a single place
                - title: Improving our product capabilities
                  content: |
                    * Long term, we'll need to adapt GitLab to incorporate the design persona as a first class user within the app. From our research, we understand that design workflows are often different from the typical DevOps flow. Many companies that employ "Design First" workflows are often working so far ahead of the DevOps process that we need to consider this as its own ecosystem and key in on. We recognize that this workflow can be quite iterative and often done and presented/approved well ahead of an issue ever being written.
                    * Ideally, GitLab will support the flexibility of any design workflow, while featuring a simplified way to share of Design work. It would allow for independent design deadlines or separate milestones from engineering, and empower the Designer to easily track/follow the issue through to production.
              button:
                title: Documentation
                link: https://docs.gitlab.com/ee/ci/test_cases/index.html
            - title: Jira
              harveyball: /nuxt-images/developer-tools/harvey-balls/0.svg
              description: Jira does not offer this
  competitor_cards:
    title: "More comparisons"
    cards:
      - name: "Harness"
        icon: agile-alt
        stage: Release
        description: How does GitLab compare to Harness in the Release stage?
        link: /competition/harness/
      - name: "Digital.ai"
        icon: manage-alt-2
        stage: Manage
        description: How does GitLab compare to Digital.ai in the Manage stage?
        link: /competition/digital-ai/
      - name: "Snyk"
        icon: secure-alt-2
        stage: Secure
        description: How does GitLab compare to Snyk in the Secure stage?
        link: /competition/snyk/