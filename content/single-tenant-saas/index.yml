---
  title: 'GitLab Dedicated'
  og_itle: 'GitLab Dedicated'
  description: The speed, efficiency and security of a DevSecOps platform available as a single-tenant SaaS.
  twitter_description: The speed, efficiency and security of a DevSecOps platform available as a single-tenant SaaS.
  og_description: The speed, efficiency and security of a DevSecOps platform available as a single-tenant SaaS.
  og_image: /nuxt-images/single-tenant-saas/open-graph-gitlab-dedicated.jpeg
  twitter_image: /nuxt-images/single-tenant-saas/open-graph-gitlab-dedicated.jpeg
  components:
    - name: 'security-content-block'
      data:
        padded: true
        note: 
          text: GitLab as a single-tenant SaaS
        header: 
          text: GitLab Dedicated
          typography: display1
        column_size: 12
        description:
          text: |
                Currently in limited availability - [join the waitlist](#sts-marketo-form)
        divider: true
    - name: 'security-resources-feature'
      data:
        copy:
          header: 
            text: Speed. Efficiency. Compliance.
          column_size: 12
          description:
            text: |
                  With GitLab Dedicated, you can maintain complex compliance standards while also leveraging the flexibility of a SaaS solution that meets your organization's deployment needs. <br>
                  Each single-tenant instance is fully managed and hosted by GitLab with data isolation and residency. This enables you to increase operational efficiencies and focus on more business-critical tasks, while achieving speed and agility in your software development lifecycle.
        resources:
          column_size: 5
          items:
            - text: Full data and source code IP isolation and residency
              icon:
                name: check
                variant: product
            - text: Fully managed and deployed by GitLab in your region of choice
              icon: 
                name: check
                variant: product
            - text: Additional data protection and isolation possible through private networking
              icon: 
                name: check
                variant: product
    - name: 'sts-marketo-form'
      data:
        title: Join the waitlist
        description: Complete this form to learn more about GitLab Dedicated or join our limited availability waitlist
        form:
          form_id: 3226
          form_header: ''
          datalayer: GitLabDedicated
          form_required_text: ''
    - name: 'solutions-resource-cards'
      data:
        column_size: 4
        cards:
          - icon:
              name: blog
              alt: Blog Icon
              variant: marketing
            event_type: "Blog"
            header: "Introducing GitLab Dedicated, our new single-tenant SaaS offering"
            link_text: "Learn more"
            href: "https://about.gitlab.com/blog/2022/11/30/introducing-gitlab-dedicated/"
            image: "/nuxt-images/single-tenant-saas/dedicated-blog-header.png"
            data_ga_name: "learn more about introducing gitlab dedicated, our new single-tenant saas offering"
            data_ga_location: resource cards
          - icon:
              name: article
              alt: Article Icon
              variant: marketing
            event_type: "Press Release"
            header: "GitLab Dedicated Launches to Meet Organizations’ Complex Compliance Requirements"
            link_text: "Learn more"
            href: "https://about.gitlab.com/press/releases/2022-11-30-gitlab-dedicated-launches-to-meet-complex-compliance-requirements.html"
            image: "/nuxt-images/resources/fallback/img-fallback-cards-infinity.png"
            data_ga_name: "gitlab dedicated launches to meet organizations’ complex compliance requirements"
            data_ga_location: resource cards
          - icon:
              name: report
              alt: Report Icon
              variant: marketing
            event_type: "Survey"
            header: "2022 Global DevSecOps Survey"
            link_text: "Learn More"
            image: "/nuxt-images/single-tenant-saas/survey-card-thumb.jpeg"
            href: "/developer-survey/"
            data_ga_name: "2022 global devsecops survey"
            data_ga_location: resource cards
    - name: 'security-cta-section'
      data:
        layout: "dark"
        cards:
          - title: Review our product direction for the GitLab Dedicated offering
            icon: 
              name: news
              slp_color: surface-700
            link:
              text: Learn More
              url: https://about.gitlab.com/direction/saas-platforms/dedicated/
              data_ga_name: learn more
              data_ga_location: body
          - title: See the features available in GitLab Dedicated limited availability today
            icon: 
              name: monitor-gitlab
              slp_color: surface-700
            link:
              text: Learn More
              url: https://docs.gitlab.com/ee/subscriptions/gitlab_dedicated/
              data_ga_name: learn more
              data_ga_location: body

    